#!/bin/env ruby
require 'sinatra'
require 'sinatra/cookies'
require 'jwt'

set :public_folder, 'public'
set :port, 9000
set :show_exceptions, false

SECRET = rand(36**8).to_s(36)

post '/login' do
  response.set_cookie :token, value: JWT.encode(JSON.parse(request.body.read), SECRET)
end

get '/static/ruby_page' do
  token = JWT.decode cookies[:token], SECRET
  "<h1>Ruby rules the world! #{Time.now}</h1><a href='/1.html'>Home</a><pre>#{token}</pre>"
end

not_found do
  halt 404 if request.path_info =~ /\.(js|vue|ico)$/ || request.path_info =~ %r{^/static}
  send_file File.join(settings.public_folder, 'index.html')
end

error { env['sinatra.error'].message }
