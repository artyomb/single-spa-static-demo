import URL from 'url'

class StatisPages {
    load_page(href) {
        let path = URL.parse(href).pathname;
        const regex = new RegExp(`^${this.url_prefix}`, "gm");

        if (regex.test(path)) {
            console.log('Statis pages load: ', href)
            path = path.replace(regex, this.server_base);

            fetch(path, {method: "GET", mode: 'no-cors'})
                .then( (response)=> {
                    if (response.status !== 200) {
                        response.text().then(text => {
                            this.domElement.innerHTML = '<pre>' + response.status + "\n" + response.statusText + "\n" + text + "</pre>";
                        })
                        return Promise.reject(new Error(response.statusText));
                    }
                    return response.text();
                })
                .then((data_html)=> {
                    this.domElement.innerHTML = data_html;
                })
                .catch((err)=> {
                    this.domElement.innerHTML = '<pre>' + err.message + "\n" + path + "</pre>";
                });
        } else {
            this.domElement.innerHTML = `<a href="/1.html">Static pages</a>`
        }
    }

    mount(opts) {
        if (typeof opts.domElement === 'string') {
            opts.domElement = document.querySelector(opts.domElement);
        }
        this.mounted = true
        this.domElement = opts.domElement
        this.url_prefix = opts.url_prefix
        this.server_base = opts.server_base

        return Promise.resolve().then(() => {
            window.addEventListener('single-spa:routing-event', evt => {
                if (this.mounted) {
                    this.load_page(evt.detail.newUrl);
                }
            });

            this.domElement.addEventListener('click', function (event) {
                let href = event.target.getAttribute('href');
                if (href) {
                    event.preventDefault();
                    let current = URL.parse(document.location.href);
                    let url = URL.parse(href[0] === '/' ? opts.url_prefix + href : current.resolve(href));
                    opts.singleSpa.navigateToUrl(url.pathname);
                }
            })
        })
    }

     unmount(opts) {
        this.mounted = false
        return Promise.resolve().then(() => {
            this.domElement.innerHTML = '';
        });
    }
}

export default function CreateStaticPages() {
    let spages = new StatisPages;
    return {
        bootstrap: ()=>Promise.resolve(),
        mount: spages.mount.bind(spages),
        unmount: spages.unmount.bind(spages)
    }
}
