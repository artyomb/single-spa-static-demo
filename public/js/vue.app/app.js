import Vue from "vue"
import VueRouter from "vue-router"
import singleSpaVue from "single-spa-vue"
import httpVueLoader from "http-vue-loader"
import URL from 'url'

const Com = httpVueLoader(URL.resolve(import.meta.url, 'Com.vue'));
const App = httpVueLoader(URL.resolve(import.meta.url, 'App.vue'));

Vue.use(VueRouter);
const routes = [
    { name: "ACom", path: "/com", component: Com, children: [] }
];
const router = new VueRouter({ mode: "history", base: '/', routes });

const vueLifecycles = singleSpaVue({ Vue, appOptions: { render: (h)=>h(App), router } });

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;