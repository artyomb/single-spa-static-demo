import Cookies from 'js-cookie'

function login(login, password, url){
    return fetch(url,{ method: 'post',  body: JSON.stringify({login: login, password: password})})
}

function render(opts){
    console.log('OAuth: render', Cookies.get('token'))
    if (Cookies.get('token') === undefined ) {
        opts.domElement.innerHTML = `
               <form>  
                <label>Username : </label> 
                <input type="text" placeholder="Enter Login" name="login" required>  
                <label>Password : </label>   
                <input type="password" placeholder="Enter Password" name="password" required>  
                <button type="submit">Login</button>   
               </form>         
            `
        opts.domElement.querySelector('form').addEventListener('submit', e => {
            e.preventDefault()
            login(e.target.elements['login'].value, e.target.elements['password'].value, opts.login)
                .then(() => {
                    opts.singleSpa.triggerAppChange()
                })
        })
    }else{
        opts.domElement.innerHTML = `<button>Logout</button>`
        opts.domElement.querySelector('button').addEventListener('click', (e) => {
            Cookies.remove('token')
            opts.singleSpa.triggerAppChange();
        })
    }
}
export function mount(opts, mountedInstances, props) {
    return Promise.resolve().then(()=>{
        if (typeof opts.domElement === 'string'){
            opts.domElement = document.querySelector(opts.domElement);
        }
        window.addEventListener('single-spa:routing-event', e=>render(opts))
    })
}

export const bootstrap = ()=>Promise.resolve();

export function unmount(opts){
    return Promise.resolve().then(()=> {
        if (typeof opts.domElement === 'string'){
            opts.domElement = document.querySelector(opts.domElement);
            opts.domElement.innerHTML = '';
        }
    })
}