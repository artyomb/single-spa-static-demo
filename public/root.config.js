const import_map = {
    "single-spa":           "https://cdn.jsdelivr.net/npm/single-spa@5.9.0/+esm",
    "vue":                  "https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.esm.browser.min.js",
    "vue-router":           "https://cdn.jsdelivr.net/npm/vue-router@3.0.7/+esm",
    "http-vue-loader":      "https://cdn.jsdelivr.net/npm/http-vue-loader@1.4.2/+esm",
    "url":                  "https://cdn.jsdelivr.net/npm/url/+esm",
    "js-cookie":            "https://cdn.jsdelivr.net/npm/js-cookie/+esm",
    //   "pubsub-js":       "https://cdn.jsdelivr.net/npm/pubsub-js@1.9.4/+esm",

    "single-spa-vue":       "https://cdn.jsdelivr.net/npm/single-spa-vue@2.5.1/+esm",
    //"single-spa-vue-parcel": "https://cdn.jsdelivr.net/npm/single-spa-vue@2.5.1/dist/esm/parcel.js",

    "oauth.app":            "/js/oauth.app/app.js",
    "js.app":               "/js/js.app/app.js",
    "static-pages.app":     "/js/static-pages.app/app.js",
    "vue.app":              "/js/vue.app/app.js",
    "mod":                  "/js/mod.js",
    // "api":               "/js/api.js"
}

document.head.appendChild(Object.assign(document.createElement('script'), {
    type: 'importmap', innerHTML: JSON.stringify( { "imports": import_map } )
}));

function append_element(tag, opts={}){
    document.head.appendChild(Object.assign(document.createElement(tag), { async:'true', ...opts}))
}

// Object.values(import_map).forEach(url => append_element('link', {href: url, rel:'preload', as:'script', crossOrigin:"anonymous" }))

append_element('script',{src: 'https://cdn.jsdelivr.net/npm/es-module-shims@1.5.9'})
append_element('script',{src: '/apps.config.js', type: 'module'})
