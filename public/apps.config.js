import * as singleSpa from 'single-spa'
import Cookies from 'js-cookie'

const authenticate = ()=>( Cookies.get('token') !== undefined )

singleSpa.start();

singleSpa.registerApplication({
    name: 'oauth.app', app: () => import('oauth.app'),
    activeWhen: '/',
    customProps: {domElement: '#oauth', login: '/login', }
});

import CreateStaticPages from 'static-pages.app'

singleSpa.registerApplication({
    name: 'static-pages.app', app: CreateStaticPages(),
    activeWhen: authenticate,
    customProps: {domElement: '#static-pages', server_base: '/static', url_prefix: '/pages'}
});

singleSpa.registerApplication({
    name: 'static-pages2.app', app: CreateStaticPages(),
    activeWhen: authenticate,
    customProps: {domElement: '#static-pages2', server_base: '/static', url_prefix: '/2pages'}
});

singleSpa.registerApplication({
    name: 'vue.app', app: () => import('vue.app'),
    activeWhen: authenticate,
    customProps: {domElement: '#vue-app', some: 'value'}
});

("single-spa:before-app-change " +
    "single-spa:before-routing-event " +
    "single-spa:before-mount-routing-event " +
    "single-spa:before-first-mount " +
    "single-spa:first-mount " +
    "single-spa:app-change " +
    "single-spa:routing-event").split(" ").forEach((e)=>{window.addEventListener(e, e=>{
    console.log('SingleSpa: ', e.type, e.detail?.newUrl, e.detail?.newAppStatuses)
})});
