# [single-spa](https://github.com/single-spa/single-spa) Static demo

No Webpack, No SystemJS

+ [es-module-shims](https://github.com/guybedford/es-module-shims)
+ Ruby [Sinatra Server](https://github.com/sinatra/sinatra) (Serves static files, no build)
+ Vue App (in-browser, no build)
+ Vanilla JS App (proxy static pages)
+ Vanilla JS Auth App (JWT login)
+ JWT Authentication
